import web
from random_post import get_random_joshreads_post

urls = (
  '/', 'Index'
)

app = web.application(urls, globals())

render = web.template.render('templates/')

class Index(object):
    def GET(self):

        try:
            post,date = get_random_joshreads_post()
            return render.index(
                post_title = post['title'],
                post_url = post['url'],
                post_date = date
            )
        except Exception as e:
            return render.error(error = e)

if __name__ == "__main__":
    app.run()
