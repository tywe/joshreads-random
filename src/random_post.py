from datetime import date
from radar import random_datetime
from BeautifulSoup import BeautifulSoup
import urllib2

def get_webpage_source(url):
    header = {
        'User-Agent': 'Randomizer by Tyler Wengerd - admin@tylerwengerd.com',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
        'Accept-Encoding': 'none',
        'Accept-Language': 'en-US,en;q=0.8',
        'Connection': 'keep-alive'
    }
    request = urllib2.Request(url, headers=header)
    page = urllib2.urlopen(request)
    html = page.read()
    return html

def get_article(html):
    soup = BeautifulSoup(html)
    first_article = soup.article.h1.a
    article = {}
    article['url'] = first_article['href']
    article['title']= "".join(str(item) for item in first_article.contents)
    return article

def get_random_joshreads_post():
    random_date = random_datetime(start='2004-07-11', stop=date.today())
    formatted_random_date = date.strftime(random_date, '%Y/%m/%d')
    wordy_random_date = date.strftime(random_date, '%A %B %d %Y')

    url = 'https://joshreads.com/{blog_date}/'.format(
        blog_date=formatted_random_date
    )
    html = get_webpage_source(url)
    article = get_article(html)

    return article, wordy_random_date

if __name__ == "__main__":
    get_random_joshreads_post()
